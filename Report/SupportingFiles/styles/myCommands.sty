%% This is a style file for adding your own commands

\NeedsTeXFormat{LaTeX2e}

%% TRACKING
\renewcommand\CancelColor{\color{red}} % to cancel in equations
\newcommand\attention[1]{\textcolor{green} {#1}} % to highlight text

%% BEAMER
\newcommand\chapterOrPart[1]{
	\only<article>{\chapter{#1}} % define new chapter, when not in beamer mode
	\only<presentation>{\part{#1}} % define new part, because chapters do not exist in beamer
	\mode*} % \mode* = to not process text outsides frames in beamer 
\newcommand{\vspacebeamer}[1][0.5cm]{\only<presentation>{\vspace{#1}}} % add vertical space in beamer, default is 0.5cm, spaces in beamer
\newcommand\textbeamer[4]{\only<presentation>{\begin{textblock*}{#1}(#2,#3)\small #4\end{textblock*}}}
\newcommand\frameVisible[0]{\iftoggle{showResults}{all}{presentation=0}}
\newcommand\largebeamer[0]{\only<presentation>{\large}} % make large in beamer
\newcommand\Largebeamer[0]{\only<presentation>{\Large}} % make Large in beamer
\newcommand\smallbeamer[0]{\only<presentation>{\small}} % make small in beamer
\newcommand\footnotesizebeamer[0]{\only<presentation>{{\footnotesize}}}
\newcommand\scriptsizebeamer[0]{\only<presentation>{\scriptsize}}
\newcommand\tinybeamer[0]{\only<presentation>{\tiny}}
\newcommand\altVal[2]{#1}

\newcommand\framefig[5]{% 1: title, 2: width, 3: path, 4: file/label 5: caption
    \begin{frame}{#1}
        \begin{figure}\centering
            \includegraphics[width=#2\textwidth,height=0.9\textheight,keepaspectratio]{#3/#4}
            \caption{#5}\label{f:#4}
        \end{figure}
    \end{frame}}

%% SYMBOLS
\newcommand{\abshum}[0]{\ensuremath{x}} % absolute humidity
\newcommand{\masf}[0]{\ensuremath{y}} % mass fraction 
\newcommand{\mdot}[0]{\ensuremath{\dot{m}}} % mass flow rate 
\newcommand{\molf}[0]{\ensuremath{Y}} % mole fraction 
\newcommand{\pres}[0]{\ensuremath{p}} % pressure 
\newcommand{\psat}[0]{\ensuremath{\pres_\up{sat}}} % saturation pressure
\newcommand{\qdot}[0]{\ensuremath{\dot{Q}}} % thermal power
\newcommand{\relhum}[0]{\ensuremath{\varphi}} % relative humidity
\newcommand{\temp}[0]{\ensuremath{\T}} % temperature
\newcommand{\thermcond}[0]{\ensuremath{\lambda}} % thermal conductivity
\newcommand{\vdot}[0]{\ensuremath{\dot{\mathcal{V}}}} % volume flow rate
\newcommand{\vel}[0]{\ensuremath{V}} % velocity
\newcommand{\vol}[0]{\ensuremath{\mathcal{V}}} % volume
\newcommand{\specvol}[0]{\ensuremath{{v}}}  % specific volume
\newcommand{\wdot}[0]{\ensuremath{\dot{W}}} % mechanical power

%% OTHER
\newcommand\up[1]{\mathrm{#1}} % to put text in mathrm
%\newcommand{\trans}[1]{[{\footnotesize Dutch:} {#1}]} % to add Dutch translation
\newcommand{\dutch}[1]{[{\footnotesize Dutch:} {#1}]} % to add English translation
\newcommand{\english}[1]{[{\footnotesize Engels:} {#1}]} % to add English translation
\newcommand{\units}[1]{\ensuremath{\quad\up{\left[#1\right]}}} % to put units after equations
\newcommand{\unitschart}[1]{\ensuremath{\;\up{\left[#1\right]}}} % to put units after axis names

%% NOMENCLATURE
\newcommand{\acronym}[3]{#1 & #2 \\}
\newcommand{\dimensionlessnumber}[3]{$\up{#1}$ & #2 & $#3$\\}
\newcommand{\nomenclature}[4]{$#1$ & #2 & $\up{#4}$\\} % symbol, DUTCH, ENG, unit
\newcommand{\nomenclaturesubscript}[3]{#1 & #3\\}

%% CONSTANTS
\newcommand{\boltzvalue}[0]{\SI{5.67e-8}{\watt\per\square\meter\per\kelvin\tothe{4}}}
\newcommand{\cpairvalue}[0]{\SI{1.00}{\kilo\joule\per\kilogram\per\kelvin}}
\newcommand{\cpwatervalue}[0]{\SI{4.18}{\kilo\joule\per\kilogram\per\kelvin}}
\newcommand{\cwatervapvalue}[0]{\SI{1.86}{\kilo\joule\per\kilogram\per\kelvin}}
\newcommand{\cvwatervalue}[0]{\SI{4.18}{\kilo\joule\per\kilogram\per\kelvin}}
% \newcommand{\cvwatervalue}[0]{\SI{4.18}{kJ kg^-1 K^-1}}
% \newcommand{\cvwatervalue}[0]{\SI{4.18}{kJ/kgK}}

\newcommand{\Runivvalue}[0]{\SI{8.31}{\joule\per\mole\per\kelvin}}
\newcommand{\Rluchtvalue}[0]{\SI{287}{\joule\per\kilo\gram\per\kelvin}}
\newcommand{\gvalue}[0]{\SI{9.81}{\meter\per\second\squared}}
\newcommand{\normaldensity}[0]{1.29\up{\frac{kg}{Nm^3}}}
\newcommand{\rhowatervalue}[0]{\SI{1000}{\kilogram\per\cubic\meter}}
\newcommand{\normaldensityvalue}[0]{\SI{1.29}{\kilogram/{N\cubic\meter}}}

%% UNITS
\newcommand{\convcoef}[0]{\watt\per\square\meter\per\kelvin}
\newcommand{\kwe}[0]{kW$\mathrm{_e}$}
\newcommand{\kwhe}[0]{kWh$\mathrm{_e}$}
\newcommand{\mwe}[0]{MW$\mathrm{_e}$}
\newcommand{\normalcube}[0]{\ensuremath{\mathrm{Nm^3}}}

%% ABBREVIATIONS
\newcommand{\bve}[0]{\textsc{bve}} %bve
\newcommand{\bvm}[0]{\textsc{bvm}} %bvm
\newcommand{\bvme}[0]{\textsc{bvme}} %bvm
\newcommand{\cor}[1]{\textbf{#1}}
\newcommand{\cp}[0]{\ensuremath{c_p}}
\newcommand{\cprg}[0]{{c_\up{p,rg}}}
\newcommand{\cte}[0]{\up{c^{te}}}
\newcommand{\cv}[0]{{c_v}} % beter c_\up{\vol} ?
\newcommand{\CV}[0]{\up{cv}}
\newcommand{\etac}[0]{\eta^{\mathrm{isen}}_\mathrm{c}}
\newcommand{\etacarnot}[0]{\ensuremath{\eta_{\up{Carnot}}}}
\newcommand{\etaisen}[0]{\ensuremath{\eta^{\up{isen}}}}
\newcommand{\etapol}[0]{\ensuremath{\eta^{\up{pol}}}}
\newcommand{\etat}[0]{\eta^{\mathrm{isen}}_\mathrm{t}}
\newcommand{\etathermisch}[0]{\ensuremath{\eta^{\up{th}}}}
\newcommand{\Nusselt}[0]{\up{N\!u}}
\newcommand{\Prandtl}[0]{\up{Pr}}
\newcommand{\pv}[0]{$\pres\vol$-diagram}
\newcommand{\ph}[0]{$\pres h$-diagram}
\newcommand{\rpm}[0]{$\ensuremath{N}$}
\newcommand{\Reynolds}[0]{\up{Re}}
\newcommand{\ts}[0]{$T\!s$-diagram}
\newcommand{\tdb}[0]{\ensuremath{T_\up{DB}}}
\newcommand{\tnb}[0]{\ensuremath{T_\up{NB}}}
\newcommand{\ud}[0]{\up{d}}
\newcommand{\viscodyn}[0]{\mu}
\newcommand{\viscokin}[0]{\nu}


%% DIFFERENTIALS
\newcommand{\dA}[0]{\ensuremath{\ud A}}
\newcommand{\dV}[0]{\ensuremath{\ud\vol}}
\newcommand{\dm}[0]{\ensuremath{\ud m}}
\newcommand{\dS}[0]{\ensuremath{\ud S}}
\newcommand{\dT}[0]{\ensuremath{\ud T}}
\newcommand{\DT}[0]{\ensuremath{\Delta\!T}}
\newcommand{\dx}[0]{\ensuremath{\ud x}}
\newcommand{\hevap}[0]{\Delta h_\up{evap}}

%% ENVIRONMENTS
% calculation example
\newcounter{example}
\newenvironment{calculationexample}[1]{\refstepcounter{example}\par\medskip\noindent\color{brown}\textbf{Example~\theexample:~#1}\par}{}%\smallskip}

\newenvironment{Solution}
{	\color{blue}}
{	\par
	%\tikz{\draw [pin](0,0) -- ++(\textwidth,0) -- ++(0,0.1cm)}
}

\theoremstyle{definition} % plain, definition, remark

\newenvironment{sitquote}{\begin{quote}} {\end{quote}}
\newenvironment{boldquote}{\begin{quote}\textbf}{\end{quote}}
\newenvironment{itemshort}{\begin{itemize} \setlength{\itemsep}{-0.10cm}} {\end{itemize}} % more dense
\newenvironment{descriptionshort}{\begin{description} \setlength{\itemsep}{-0.15cm}} {\end{description}} % more dense
\newenvironment{enumerateshort}{\begin{enumerate} \setlength{\itemsep}{-0.10cm}} {\end{enumerate}} % more dense
