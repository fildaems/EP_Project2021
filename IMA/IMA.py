#Library's
import tkinter as tk
from tkinter import filedialog
import matplotlib.pylab as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import numpy as np
import cv2 as cv
import os
#components
from components import fileImport
from components import editorColumn 
from components import guiElements 

#main program
def IMA():

    mainframe = tk.Tk()

    class fileClass:
        def __init__(self): #initialisation of filesystem
            self.filelist = []
            self.currfile = None 
            self.loadedimgs = []
            self.focusimg = None
            self.editimg = None
            self.history = []
            self.editTree = []
            self.curreditframe = None
            self.editoptions = {
                    'default stack': None, 
                    'crop': None,
                    'rgb': None, 
                    'gamma': None, 
                    'filter': {
                        'Sepia': [[0.393,0.769,0.189],[0.349,0.686,0.168],[0.272,0.534,0.131]],
                        'Pixelation': None,
                        },
                    'convLayer': {
                        'boxBlur': [[1/9,1/9,1/9],[1/9,1/9,1/9],[1/9,1/9,1/9]],
                        'gaussianBlur': [[1/16,1/8,1/16],[1/8,1/4,1/8],[1/16,1/8,1/16]],
                        'sharpen': [[0,-1,0],[-1,5,-1],[0,-1,0]],
                        'edgeDetect': [[-1,-1,-1],[-1,8,-1],[-1,-1,-1]],
                        'edgeDetect2': [[1,0,-1],[0,0,0],[-1,0,1]],
                        'verticalEdges': [[1,0,-1],[1,0,-1],[1,0,-1]],
                        'horizontalEdges': [[1,1,1],[0,0,0],[-1,-1,-1]],
                        'emboss': [[-2,-1,0],[-1,1,1],[0,1,2]],
                       #'gradient': [[-3-3j,-10j,3-3j],[-10,0,10],[-3+3j,10j,3+3j]],
                        }
                    }
            self.allowedFileTypes=['png','JPG']
            self.ConvAcc = False 
            self.ConvAccPointer = ''
        def ToggleConvMode(self, label): #function to toggle the scipy acceleration
            self.ConvAcc = not self.ConvAcc
            if self.ConvAcc:
                label.config(text='Accelerated by Scipy')
            else:
                label.config(text='')
            return
        def addFile(self, path): #method to load a file into the filesystem
            self.filelist.append({'path': path})
            file = self.filelist[-1]
            index = len(self.filelist)-1
            file['name'] = path.split('/')[-1]
            temp = plt.imread(path)
            file['ogimg'] = temp.copy() 
            file['editimg'] = temp.copy() 
            file['editTree'] = [] 
            file['editFrame'] = None
            file['preview'] = None
            self.currfile = index
            return index 

        def changeWorkingFrame(self, frame, index): #method for changing to the newly selected files editcolumn
            self.curreditframe = frame
            if not index == None:
                self.filelist[index]['editFrame'] = frame
                self.currfile = index
               #self.curreditframe.pack(side='bottom', fill='both', expand=True)
                self.curreditframe.grid(row=1,column=2, padx=0, pady=3, sticky='nesw')
                tempTree = self.filelist[index]['editTree']
                for edit in tempTree:
                    edit['updFunc'](edit['value'])
            return
        def updatePreviews(self): #method for updating all the previews in the previewcolumn
            for i in self.filelist:
                try:
                    i['preview'].plotImage()
                except:
                    pass
            return

    class imageFigure:
        def __init__(self, files, screen): #initialisation of the editor-live-view
            self.files = files
            self.fig = plt.Figure()
            self.img = self.fig.add_subplot(1,1,1)
            self.img.set_axis_off()
            self.img.autoscale(tight=True)
            self.canvas = FigureCanvasTkAgg(self.fig, master=screen)
            self.canvas.draw()
            self.canvas.get_tk_widget().pack(expand=True, fill='both')
            return
        def plotImage(self, clearim=False): #method for updating plotted image
            if clearim:
                self.img.clear()
                self.img.imshow(
                        [[[0,0,0,1],[0,0,0,1],[0,0,0,1]],
                         [[0,0,0,1],[0,0,0,1],[0,0,0,1]],
                         [[0,0,0,1],[0,0,0,1],[0,0,0,1]]]
                        )
                self.img.set_axis_off()
                self.canvas.draw()
                return
            self.img.clear()
            self.img.imshow(self.files.filelist[self.files.currfile]['editimg'])
            self.img.set_axis_off()
            self.canvas.draw()
            return

    class imagePreview: #class used for every loaded image-preview
        def __init__(self, files, images, screen, index): #initialisation of the preview for the corresponding file
            self.index = index
            self.files = files
            self.fig = plt.Figure()
            self.img = self.fig.add_subplot(1,1,1)
            self.img.set_axis_off()
            self.img.autoscale(tight=True)
            self.frame = tk.Frame(master=screen)
            self.frame.pack(side='top',fill='x', expand=True)
            self.canvas = FigureCanvasTkAgg(self.fig, master=self.frame)
            self.canvas.draw()
            self.canvwid = self.canvas.get_tk_widget()
            self.canvwid.pack(side='top', pady=10, fill='x', expand=True)
            self.selectbut =  tk.Button(master=self.frame, text='Select', foreground='#000000', \
                    command=self.selectimg)
            self.selectbut.pack(side='left', fill='x', expand=True)
            self.savebut = tk.Button(master=self.frame, text='Save', foreground='#000000', \
                    command=self.saveimg)
            self.savebut.pack(side='right', fill='x', expand=True)
            self.selectimg()
            return
        def plotImage(self): #method for updating the preview
            image = files.filelist[self.index]['editimg']
            self.img.clear()
            self.img.imshow(image)
            self.img.set_axis_off()
            self.canvas.draw()
            return
        def selectimg(self): #method for selecting image in the previewcolumn
            currframe = files.curreditframe
            selectedframe = files.filelist[self.index]['editFrame']
            if selectedframe == currframe:
                files.updatePreviews()
                return
            if not currframe == None:
                currframe.grid_forget()
            if selectedframe == None:
                selectedframe = createEditColumn()
            files.changeWorkingFrame(selectedframe, self.index)
            images.plotImage()
            files.updatePreviews()
            return
        def saveimg(self): #method for saving and removing image from app 
            path = filedialog.asksaveasfile(mode='w', defaultextension='.png')
            path = str(path).split('=')[1][0:-4]
            name = path.split('/')[-1]
            path = path[0:-len(name)]
            path = os.path.join(path, name)[1:-2]
            img = np.array(files.filelist[self.index]['editimg'])
            imgcop = img.copy()
            img[:,:,0]=imgcop[:,:,2]
            img[:,:,1]=imgcop[:,:,1]
            img[:,:,2]=imgcop[:,:,0]
            img = cv.convertScaleAbs(img, alpha=(255.0))
            cv.imwrite(path, img)
            files.filelist.pop(self.index)
            files.curreditframe.grid_forget()
            self.canvwid.pack_forget()
            self.canvwid.destroy()
            self.selectbut.pack_forget()
            self.selectbut.destroy()
            self.savebut.pack_forget()
            self.savebut.destroy()
            images.plotImage(clearim=True)
            return

    #set-up main screen
    mainframe.columnconfigure([0], weight=5, minsize=25)
    mainframe.columnconfigure([1], weight=25, minsize=75)
    mainframe.columnconfigure([2], weight=5, minsize=25)
    mainframe.rowconfigure([0], weight=9, minsize=30)
    mainframe.rowconfigure([1], weight=100, minsize=75)
    mainframe.rowconfigure([2], weight=2, minsize=10)
    
    topBar = tk.Frame(master=mainframe, bg='#adadad')
    statusBar = tk.Frame(master=mainframe, bg='#adadad')
    mainscreen = tk.Frame(master=mainframe, bg='#d6d6d6')
    leftColumn = tk.Frame(master=mainframe, bg='#777777')
    rightColumn = tk.Frame(master=mainframe, bg='#404040')
    
    #mainframe layout
    topBar.grid(row=0,column=0, columnspan=2, padx=0, pady=0, sticky='nesw')
    statusBar.grid(row=0, column=2, padx=0, pady=0, sticky='nesw')
    mainscreen.grid(row=1,column=1, padx=2, pady=3, sticky='nesw')
    leftColumn.grid(row=1,column=0, padx=0, pady=3, sticky='nesw')
    rightColumn.grid(row=1,column=2, padx=0, pady=3, sticky='nesw')
    mainscreen.rowconfigure([0], weight=1, minsize=10)
    mainscreen.rowconfigure([1], weight=525, minsize=10)
    mainscreen.columnconfigure([0], weight=1)
    
    #class initiation
    files = fileClass()
    images = imageFigure(files, mainscreen)

    files.changeWorkingFrame(rightColumn, None)
    
    #topbar setup
    accPath = tk.Label(master=statusBar, text='', background='#adadad', foreground='#000000') 
    accPath.pack(side='right', fill='y', expand=True)
    textPath = tk.Label(master=topBar, text=files.currfile, background='#adadad', foreground='#000000') 
    textPath.pack(fill='both', expand=True)
    
    #menubar setup
    menubar = tk.Menu(mainframe)
    filemenu = tk.Menu(menubar)
    filemenu.add_command(label="Toggle Scipy Acceleration", command= lambda: files.ToggleConvMode(accPath))
    menubar.add_cascade(label='Convolution', menu=filemenu)

    #previewColumn setup
    imPrevPane, bigframe = guiElements.scrollContainer(leftColumn)
    PreviewPack = (imPrevPane, imagePreview)
    fileImport.uploadButton(leftColumn, textPath, PreviewPack, files, images, 1, 1, 0)
    bigframe.pack(side='bottom', fill='both', expand=True)
    
    #editColumn setup
    sliders = []
    def createEditColumn():
        column = tk.Frame(master=mainframe, bg='#404040')
        column.grid(row=1,column=2, padx=0, pady=3, sticky='nesw')
        frame, bigframe = guiElements.scrollContainer(column)
        editorColumn.editDropdown(column, frame, images, files, sliders)
        bigframe.pack(side='bottom', fill='both', expand=True)
        return column 

    #window setup
    mainframe.title("ImageManipulationApplication")
    mainframe.geometry("960x540")
    mainframe.minsize(480,270)
    mainframe.config(menu=menubar)
    mainframe.mainloop()

    #return statement for main function
    return


#check if run from file
if __name__ == '__main__':
    IMA()

