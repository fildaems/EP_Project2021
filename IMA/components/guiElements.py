#Library's
import tkinter as tk
import matplotlib.pylab as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import numpy as np

def scrollContainer(screen): #method to create a scrollable window in tkinter
        def onFrameConfigure(event, canvas):
            canvas.configure(scrollregion=canvas.bbox("all"))

        def onCanvasConfigure(event, canvas, window):
            canvas_width = event.width 
            canvas.itemconfig(window, width=canvas_width)
        mainframe = tk.Frame(screen)
        canvas = tk.Canvas(mainframe, width=150, height=50000, borderwidth=0, background='#404040')
        verticalsb = tk.Scrollbar(mainframe, orient='vertical', command=canvas.yview)
        canvas.configure(yscrollcommand=verticalsb.set)
        canvas.pack(side="left", fill="both", expand=True)
        verticalsb.pack(side='left', fill="y", expand=False)
        frame = tk.Frame(canvas, background='#ffffff')
        window = canvas.create_window((0,0), window=frame, anchor="nw",)
        canvas.bind("<Configure>", lambda event, canvas=canvas, window=window: \
                onCanvasConfigure(event, canvas, window))
        frame.bind("<Configure>", lambda event, canvas=canvas: onFrameConfigure(event, canvas))
        return frame, mainframe
    
'''
This method was sourced on stackoverflow. A lot of modification are made to make it compatible with our application, but a small part of the main structure was not completely original
'''
