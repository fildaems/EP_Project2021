import tkinter as tk
from tkinter import filedialog
import matplotlib.pylab as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import numpy as np

def uploadButton(screen, textPath, PreviewPack, files, images, rowno=0, colno=0, pad=0):
    uploadButton = tk.Button(screen, text='Choose File', background='#777777', foreground='#000000', command= lambda: \
            importFile(files, images, textPath, PreviewPack))
    uploadButton.pack(side='top', fill='both', expand=True, padx=5, pady=5)

def importFile(files, images, textPath, PreviewPack, devlist=False):
    imPrevPane, imagePreview = PreviewPack
    path = filedialog.askopenfilename(initialdir='assets') 
    for i in files.filelist: #check is already imported
        if i['path'] == path:
            tk.messagebox.showerror(title='File already imported!', message=f'The selected file is already imported.')
            return
    ext = path.split('.')[-1]
    if not ext in files.allowedFileTypes: #check if selected file is allowed
        tk.messagebox.showerror(title='File extension not allowed!', message=f'.{ext}-files cannot be imported.')
        return
    index = files.addFile(path) #add file to filesystem
    textPath.config(text=files.filelist[files.currfile]['name']) #change text in topbar to selected file
    imageclass = imagePreview(files, images, imPrevPane, index) #create previewpane for selected file
    files.filelist[index]['preview'] = imageclass #assign previewpane to file in filesystem
    imageclass.plotImage() #plot image in preview column
    images.plotImage() #plot image in mainscreen
    return
