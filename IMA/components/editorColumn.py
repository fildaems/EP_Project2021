import tkinter as tk
from tkinter import filedialog
import matplotlib.pylab as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import numpy as np
from scipy import signal
import math

textcolor = '#000000'

def editDropdown(screen, oldframe, images, files, sliders):
    optionList = files.editoptions.keys()
    var = tk.StringVar()
    var.set("Add new edit...")

    def addEdit(item):
        editCreateList = {'default stack': defaultStack, 'crop': cropSliders, 'rgb': RGBSliders, 'gamma': gammaSlider, 'filter': filterAdd, 'convLayer': convLayer,}
        editCreateList[item](frame, images, files, sliders)
        var.set("Add new edit...")
    frame = tk.Frame(master=oldframe)
    frame.pack(fill="x", expand=True)
    dropdown = tk.OptionMenu(screen, var, *optionList, command=addEdit)
    dropdown.configure(foreground=textcolor)
    dropdown.pack(anchor='n', fill='both', padx=4, pady=4, expand=True)
# addEdit('default stack')
    return

def defaultStack(screen, images, files, sliders):
    RGBSliders(screen, images, files, sliders)
    gammaSlider(screen, images, files, sliders)
    filterAdd(screen, images, files, sliders)
    convLayer(screen, images, files, sliders)
    cropSliders(screen, images, files, sliders)
    return

def cropSliders(screen, images, files, sliders):
    edit = {"type": 'crop', 'value': {'top': 100, 'right': 100, 'bottom': 100, 'left': 100} }
    files.filelist[files.currfile]['editTree'].append(edit)
    frame = tk.Frame(master=screen, highlightbackground='black', highlightthickness=1)
    frame.pack(fill='x', padx=4, expand=True)
    label =tk.Label(master=frame, text='Crop Image', foreground=textcolor)
    label.pack()
    bigSliderPack = []
    sliderPack = slider(frame, edit,   images, files, 'top') 
    updatetop = sliderPack[-1]
    sliders.append(sliderPack) 
    bigSliderPack.append(sliderPack)
    sliderPack = slider(frame, edit,  images, files, 'left') 
    updateleft = sliderPack[-1]
    sliders.append(sliderPack) 
    bigSliderPack.append(sliderPack)
    sliderPack = slider(frame, edit, images, files, 'right') 
    updateright = sliderPack[-1]
    sliders.append(sliderPack) 
    bigSliderPack.append(sliderPack)
    sliderPack = slider(frame, edit, images, files, 'bottom') 
    updatebottom = sliderPack[-1]
    sliders.append(sliderPack) 
    bigSliderPack.append(sliderPack)
    controlComp(frame, bigSliderPack, edit, files, images)
    def cropupdate(count):
        updatetop(count['top'])
        updateright(count['right'])
        updatebottom(count['bottom'])
        updateleft(count['left'])
        return
    files.filelist[files.currfile]['editTree'][-1]['updFunc'] = cropupdate
    return

def RGBSliders(screen, images, files, sliders):
    edit = {"type": 'rgb', 'value': {'red': 100, 'green': 100, 'blue': 100} }
    files.filelist[files.currfile]['editTree'].append(edit)
    frame = tk.Frame(master=screen, highlightbackground='black', highlightthickness=1)
    frame.pack(fill='x', padx=4, expand=True)
    label =tk.Label(master=frame, text='RGB adjustment', foreground=textcolor)
    label.pack()
    bigSliderPack = []
    sliderPack = slider(frame, edit,   images, files, 'red') 
    updatered = sliderPack[-1]
    sliders.append(sliderPack) 
    bigSliderPack.append(sliderPack)
    sliderPack = slider(frame, edit, images, files, 'green') 
    updategreen = sliderPack[-1]
    sliders.append(sliderPack) 
    bigSliderPack.append(sliderPack)
    sliderPack = slider(frame, edit,  images, files, 'blue') 
    updateblue = sliderPack[-1]
    sliders.append(sliderPack) 
    bigSliderPack.append(sliderPack)
    controlComp(frame, bigSliderPack, edit, files, images)
    def rgbupdate(count):
        updatered(count['red'])
        updategreen(count['green'])
        updateblue(count['blue'])
        return
    files.filelist[files.currfile]['editTree'][-1]['updFunc'] = rgbupdate
    return

def gammaSlider(screen, images, files, sliders):
    edit = {"type": 'gamma', 'value': 100 }
    files.filelist[files.currfile]['editTree'].append(edit)
    frame = tk.Frame(master=screen, highlightbackground='black', highlightthickness=1)
    frame.pack(fill='x', expand=True, padx=4)
    label =tk.Label(master=frame, text='Gamma adjustment', foreground=textcolor)
    label.pack()
    sliderPack = slider(frame, edit, images, files)
    sliders.append(sliderPack) 
    controlComp(frame, sliderPack, edit, files, images)
    update = sliderPack[-1]
    files.filelist[files.currfile]['editTree'][-1]['updFunc'] = update
    return

def slider(screen, edit, images, files, rgbComponent=None):

    if rgbComponent == None:
       label = tk.Label(master=screen, text=str(edit['type'])+': '+str(edit['value']), foreground=textcolor)
    else:
       label = tk.Label(master=screen, text=str(rgbComponent)+': '+str(edit['value'][rgbComponent]), foreground=textcolor)
    label.pack(side=tk.TOP)

    def updateText(count):
        if rgbComponent== None:
            label.config(text=str(edit['type'])+': '+str(count)) 
        else:  
            label.config(text=str(rgbComponent)+': '+str(count)) 

    def update(count):
        count = int(count)
        if not rgbComponent == None:
            edit['value'][rgbComponent] = count/100
            if count<100:
                updateText(str(count-100)+'%')
            else:
                updateText('+'+str(count-100)+'%')
        elif edit['type'] == 'gamma':
            if count == 0:
                edit['value'] = 0
            else:
                count -= 50
                count /= 50
                count = 8 ** count
                edit['value'] = count
            updateText('%.2f' % count)
        elif edit['value'][0] == 'Pixelation':
            edit['value'][1] = count
            updateText(count)
            pass
        updateImg(images, files)

    maxval = 100
    if edit['type'] == 'rgb':
        maxval = 200
    if rgbComponent == 'bottom':
        slider = tk.Scale(master=screen, orient="vertical", command=update, showvalue=0, to=maxval)
# slider.pack(side=tk.TOP)
    elif rgbComponent == 'top':
        slider = tk.Scale(master=screen, orient="vertical", command=update, showvalue=0, from_=maxval, to=0)
# slider.pack(side=tk.TOP)
    elif rgbComponent == 'left':
        slider = tk.Scale(master=screen, orient="horizontal", command=update, showvalue=0, from_=maxval, to=0)
# slider.pack(side=tk.LEFT)
    elif rgbComponent == 'right':
        slider = tk.Scale(master=screen, orient="horizontal", command=update, showvalue=0, to=maxval)
# slider.pack(side=tk.RIGHT)
    else:
        slider = tk.Scale(master=screen, orient="horizontal", command=update, showvalue=0, to=maxval)
# slider.pack(side=tk.TOP)
    slider.pack(side=tk.TOP)
    if edit['type'] == 'crop':
        slider.set(maxval)
    else:
        slider.set(maxval/2)
    if type(edit['value']) == list and edit['value'][0]:
        slider.set(25)

    def setupdate(count):
        if not rgbComponent == None:
            count *= 100
            if count<100:
                updateText(str(count-100)+'%')
                slider.set(count) 
            else:
                updateText('+'+str(count-100)+'%')
                slider.set(count) 
        elif edit['type'] == 'gamma':
            if not count == 0:
                count = math.log(count,8) 
                count *= 50
                count += 50
            slider.set(count) 
            updateText('%.2f' % count)
        elif edit['value'][0] == 'Pixelation':
            slider.set(count) 
            updateText(count)
            pass
        return

    return (slider, maxval, label, setupdate)

def filterAdd(screen ,images, files, sliders):
    edit = {"type": 'filter', 'value': [None, 20]}
    files.filelist[files.currfile]['editTree'].append(edit)
    frame = tk.Frame(master=screen, highlightbackground='black', highlightthickness=1)
    frame.pack(fill='x', padx=4, expand=True)
    label =tk.Label(master=frame, text='Add Filter', foreground=textcolor)
    label.pack()
    update = filterwidget(frame, edit, images, files)
    files.filelist[files.currfile]['editTree'][-1]['updFunc'] = update
    controlComp(frame, None, edit, files, images)
    return

def filterwidget(frame, edit, images, files):
    widgettype=edit['type']
    var = tk.StringVar()
    var.set('Choose '+widgettype+'...')
    optionList = files.editoptions[widgettype].keys()

    def filterselected(item):
        if edit['value'][0] == item:
            return
        edit['value'][0] = item
        showSlider(edit)
        updateImg(images, files)
        return

    def showSlider(edit):
        if edit['value'][0] == 'Pixelation':
            sliderreturn, maxval, label, update = slider(frame, edit, images, files)
            edit['value'].append((sliderreturn, label))
        else:
            try:
                for i in edit['value'][2]:
                    i.pack_forget()
            except:
                pass
        return

    def update(selection):
        var.set(selection[0])
        return

    filterselect = tk.OptionMenu(frame, var, *optionList, command=filterselected)
    filterselect.configure(foreground=textcolor)
    filterselect.pack(fill=tk.BOTH)
    return update

def convLayer(screen, images, files, sliders):
    edit = {"type": 'convLayer', 'value': [None,None]}
    files.filelist[files.currfile]['editTree'].append(edit)
    frame = tk.Frame(master=screen, highlightbackground='black', highlightthickness=1)
    frame.pack(fill='x', padx=4, expand=True)
    label =tk.Label(master=frame, text='Add Convolution Layer', foreground=textcolor)
    label.pack()
    update = filterwidget(frame, edit, images, files)
    files.filelist[files.currfile]['editTree'][-1]['updFunc'] = update
    controlComp(frame, None, edit, files, images)
    return

def controlComp(frame, resetData, edit, files, images):
    def resetEdit(sliderPack=resetData):
        if type(sliderPack) == list:
            for i in sliderPack:
                resetEdit(i) 
        else:
            slider, maxval, label, updatefunc = sliderPack
            if edit['type']=='crop':
                slider.set(maxval)
            else:
                slider.set(maxval/2)
    def deleteEdit():
        files.filelist[files.currfile]['editTree'].remove(edit)
        updateImg(images, files)
        frame.pack_forget()
        frame.destroy()
        return
    if not resetData == None:
        resetButton = tk.Button(master=frame, text='reset', command=resetEdit, foreground=textcolor)
        resetButton.pack(expand=True, fill='both', side='left')
    deleteButton = tk.Button(master=frame, text='delete', command=deleteEdit, foreground=textcolor)
    deleteButton.pack(expand=True, fill='both', side='bottom')
    return resetEdit

def updateImg(images, files):
    cols = {'red': 0, 'green': 1, 'blue': 2}

    startimg, resultimg = None, np.array(files.filelist[files.currfile]['ogimg'].copy())

    def cropEdit(edit, resultimg):
        height, width = startimg.shape[0:2]
        change = edit['value']
        top     = 1-change['top']
        top     = int(top*height) 
        bottom  = change['bottom']
        bottom2     = int(bottom*height) 
        if top == height:
            top -= 1
        if bottom2<=top:
            bottom = top+2
        else:
            bottom = bottom2
        left    = 1-change['left']
        left     = int(left*width) 
        right   = change['right']
        right2     = int(right*width) 
        if left == width:
            left -= 1
        if right2<=left:
            right = left+2 
        else:
            right = right2
        resultimg = startimg[top:bottom,left:right,:]
        return resultimg

    def rgbEdit(edit, resultimg):
        for col, value in edit['value'].items():
            chan = cols[col]
            editLayer = np.array(startimg[:,:,chan].copy())
            editLayer *= edit['value'][col]
            resultimg[:,:,chan] = editLayer.copy()
        return resultimg

    def gammaEdit(edit, resultimg):
        for col, chan in cols.items():
            editLayer = np.array(startimg[:,:,chan].copy())
            if edit['value'] == 0:
                editLayer = editLayer * 0
            else:
                editLayer **= (1/edit['value'])
            resultimg[:,:,chan] = editLayer.copy()
        return resultimg
    
    def filterEdit(edit, resultimg):
        if edit['value'][0] == 'Sepia':
            matrix = np.array(files.editoptions['filter'][edit['value'][0]])
            for chan in cols.values():
                resultimg[:,:,chan] =   matrix[chan,0]*startimg[:,:,0] + \
                                        matrix[chan,1]*startimg[:,:,1] + \
                                        matrix[chan,2]*startimg[:,:,2]

        if edit['value'][0] == 'Pixelation':
            blocksize = edit['value'][1]
            if blocksize <2:
                return resultimg.copy()
            height, width = startimg.shape[0:2]
            remainH, remainW = height%blocksize , width%blocksize
            noBlocksH, noBlocksW = int((height-remainH)/blocksize), int((width-remainW)/blocksize)
            for col, chan in cols.items():
                editLayer=startimg[:,:,chan].copy()
                startH, endH, startW, endW = 0,0,0,-1
                for h in range(noBlocksH):
                    startH = h*blocksize
                    endH = (h+1)*blocksize
                    for w in range(noBlocksW):
                        startW, endW = w*blocksize, (w+1)*blocksize
                        noblocks = (endW-startW) * (endH-startH)
                        editLayer[startH:endH,startW:endW] = \
                                np.sum(editLayer[startH:endH,startW:endW])/noblocks
                    # do end of row pixalation with remainW 
                    if remainW > 0:
                        startW, endW = endW, endW+remainW
                        noblocks = (endW-startW) * (endH-startH)
                        editLayer[startH:endH,startW:endW] = \
                                np.sum(editLayer[startH:endH,startW:endW])/noblocks
                #do final remain row pixalation with remainH
                if remainH > 0:
                    startH, endH = endH, endH+remainH
                    for w in range(noBlocksW):
                        startW, endW = w*blocksize, (w+1)*blocksize
                        noblocks = (endW-startW) * (endH-startH)
                        editLayer[startH:endH,startW:endW] = \
                                np.sum(editLayer[startH:endH,startW:endW])/noblocks
                if remainH>0 and remainW>0:
                    startW, endW = endW, endW+remainW
                    noblocks = (endW-startW) * (endH-startH)
                    editLayer[startH:endH,startW:endW] = \
                            np.sum(editLayer[startH:endH,startW:endW])/noblocks

                resultimg[:,:,chan] = editLayer.copy()
        return resultimg

    def convEdit(edit, resultimg):
        if edit['value'][0] == None:
            return resultimg
        kernel = np.array(files.editoptions['convLayer'][edit['value'][0]])
        if not files.ConvAcc:
### numpy version
            shape = np.shape(startimg)
            width, height, layers = shape[0]+2, shape[1]+2, shape[2]
            borderImg = np.zeros((width, height, layers)) 
            borderImg[1:-1,1:-1,:] = startimg.copy()
            #do something with borderImg
            ogImg = borderImg.copy()
            for i in range(3):
                for h in range(1,height-1):
                    for w in range(1,width-1):
                        ogmatrix = ogImg[w-1:w+2,h-1:h+2,i] 
                        borderImg[w:w+1,h:h+1,i] = np.sum(ogmatrix*kernel) 
            resultimg = borderImg.copy()
###
        elif files.ConvAcc:
### scipy version
            ogImg = startimg.copy()
            for i in range(3):
                borderimg = signal.convolve2d(ogImg[:,:,i], kernel)[1:-1,1:-1].copy()
                resultimg[:,:,i] = borderimg.copy()
###
        return resultimg

    editmap = {'crop': cropEdit, 'rgb': rgbEdit, 'gamma': gammaEdit, 'filter': filterEdit, 'convLayer': convEdit,}  
    for index, edit in enumerate(files.filelist[files.currfile]['editTree']):
        startimg = resultimg.copy()
        resultimg = editmap[edit['type']](edit, resultimg)
    files.filelist[files.currfile]['editimg'] = resultimg.copy()
    images.plotImage()
    return




#deprecated code

def resetButton(screen, images, files, sliders):
    resetButton = tk.Button(master=screen, text='Reset Sliders', command= lambda: resetSliders(sliders))
    resetButton.pack(padx=4, pady=4)

def resetSliders(sliders):
    for slider, maxval, label in sliders:
        slider.set(maxval/2)
    return

