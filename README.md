# EP_Project2021 
Team-project for the course 'Engineering Programming' at the VUB. Deadline: 28/01/2022

This repository contains 3 different items. An Image Manipulation Applications, an Covid Data Visualizer and report describing both of these programs.
Each of these items has its own directory containing the main files, as well as supporting files.
To run the IMA or CDV please open the corresponding directory and run the IMA.py or CDV.py file with Python3.5 or later.
The report is presented in .pdf format. It is possible to find the underlying LateX files in the supporting directories.
