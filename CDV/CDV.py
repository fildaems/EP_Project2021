#Library's
import tkinter as tk
from tkinter import filedialog
import matplotlib.pylab as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import numpy as np
import csv
from datetime import datetime, timedelta
import pandas as pd

from components import guiElements

def CDV():
    mainframe = tk.Tk() #create main Tkinter window

    #configure main layout gui
    mainframe.columnconfigure([0], weight=5, minsize=25)
    mainframe.columnconfigure([1], weight=25, minsize=75)
    mainframe.rowconfigure([0], weight=100, minsize=75)
    mainscreen = tk.Frame(master=mainframe, bg='#d6d6d6')
    leftColumn = tk.Frame(master=mainframe, bg='#777777')
    mainscreen.grid(row=0,column=1, padx=2, pady=3, sticky='nesw')
    leftColumn.grid(row=0,column=0, padx=0, pady=3, sticky='nesw')
    mainscreen.rowconfigure([0], weight=1, minsize=10)
    mainscreen.rowconfigure([1], weight=525, minsize=10)
    mainscreen.columnconfigure([0], weight=1)

    #class
    class store():
        def __init__(self, screen):
            #create figure and plot
            self.fig = plt.Figure()
            self.img = self.fig.add_subplot(1,1,1)
            self.img.autoscale(tight=True)
            self.canvas = FigureCanvasTkAgg(self.fig, master=screen)
            self.canvas.draw()
            self.canvas.get_tk_widget().pack(expand=True, fill='both')

            self.mainarray = pd.read_csv('assets/owid-covid-data.csv') #read csv-file
            pd.to_datetime(self.mainarray['date']) #transfore all the dates to datetime objects
            self.selectedParams = {'metric': None, 'countries': []} #var keeping track of user selection
            self.locationList = list(self.mainarray.drop_duplicates('location').location) #all locations provided
            self.metricList = list(self.mainarray.columns)[4:] #all metrics provided
            self.barplotmetriclist = [  'total_cases', 'total_deaths', 'total_cases_per_million',       #metrics to be used with bar plot\
                                        'total_deaths_per_million', 'reproduction_rate', 'total_tests', \
                                        'total_tests_per_thousand', 'total_vaccinations', 'total_boosters',\
                                        'total_vaccinations_per_hundred', 'total_boosters_per_hundred', \
                                        'population', 'population_density', 'median_age', 'aged_65_older', \
                                        'aged_70_older', 'life_expectancy'] 
            return
        def getData(self): #method for getting the data from the mainarray, selected by the user, into a format to be plotted
            if self.selectedParams['metric'] == None or self.selectedParams['countries']==[]: #if not enough parameters are selected, dont try to format data
                return None 
            if self.selectedParams['metric'] in self.barplotmetriclist: # check for barplot or not
                #barplot
                data = self.mainarray.sort_values(by='date')
                data = data.drop_duplicates('location', keep='last')
                return data[data['location'].isin(self.selectedParams['countries'])][['location', self.selectedParams['metric']]]
            else:
                #graphplot
                data = {}
                for country in self.selectedParams['countries']:
                    data[country] = self.mainarray[self.mainarray['location']==country][['date',self.selectedParams['metric']]].copy()
                    lst = ['date', country]
                    data[country].set_axis(lst,axis=1, inplace=True)
                return data
        def setMetric(self, metric):#method for changing selected metric
            if type(metric) == str:
                self.selectedParams['metric'] = metric
                self.plotImage()
            return
        def addCountry(self, country): #adding country to selection
            if not country in self.selectedParams['countries']:
                self.selectedParams['countries'].append(country)
                self.plotImage()
            return
        def removeCountry(self, country): #removing country from selection
            if country in self.selectedParams['countries']:
                self.selectedParams['countries'].remove(country)
                self.plotImage()
            return
        def plotImage(self): #method for updating/plotting the formatted data
            self.img.clear() #clear image
            datadict = self.getData()
            if self.selectedParams['metric'] == None or self.selectedParams['countries']==[]: #check for enough parameters
                return
            elif self.selectedParams['metric'] in self.barplotmetriclist: # check for barplot or not
                datadict.plot(kind='bar', ax=self.img, x='location', y=self.selectedParams['metric'], title=self.selectedParams['metric'].replace('_', ' '), legend=False, rot=30)
            else:
                for country, data in datadict.items():
                    data.plot(ax=self.img, x='date', title=self.selectedParams['metric'].replace('_', ' '))
                self.img.legend(loc='upper left', ncol=2)
            self.canvas.draw() #update plot
            return


    #initialitation of class
    data = store(mainscreen)

    #creating the metric-selector dropdown
    optionList = data.metricList
    var = tk.StringVar()
    if data.selectedParams['metric']==None:
        var.set('Choose metric...')
    else:
        var.set(data.selectedParams['metric'])
    metricDropdown = tk.OptionMenu(leftColumn, var, *optionList, command=data.setMetric)
    metricDropdown.configure(foreground= '#000000')
    metricDropdown.pack(anchor='n', fill='x', padx=5, pady=5, expand=True)

    def makeCheckbox(screen, country): #this method creates a checkbox
        var = tk.BooleanVar()
        var.set(False)
        def checkAction():
            if var.get():
                data.addCountry(country)
            elif not var.get():
                data.removeCountry(country)
            return
        box = tk.Checkbutton(screen, text=country, background='#adadad', foreground='#000000', variable=var, onvalue=True, offvalue=False, command=checkAction)
        box.pack(side='top', fill='x', expand=False)
        return

    frame, bigframe = guiElements.scrollContainer(leftColumn) #make a scrollable frame
    for country in data.locationList: #create a checkbox for every country
        makeCheckbox(frame, country)
    bigframe.pack(side='bottom', fill='both', expand=True)

    #ensure plot on bootd
    data.plotImage()

    #window setup
    mainframe.title("CovidDataVisualisation")
    mainframe.geometry("960x540")
    mainframe.minsize(480,270)
    mainframe.mainloop()

#return statement for main function
    return

#check if run from file
if __name__ == '__main__':
    CDV()
